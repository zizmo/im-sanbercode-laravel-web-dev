<?php 
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

echo "==============ANIMAL===============</br>";
$hewan = new Animal("shaun");
echo "</br>";
echo "================FROG===============</br>";
$frog = new Frog("Buduk");
$frog->jump();
echo "</br>";
echo "</br>";
echo "================APE================</br>";
$ape = new Ape("Kera Sakti");
$ape->yell();
?>