<?php 
require_once('Animal.php');

class Ape extends Animal{
    public $legs = 2;
    public $cold_blooded = "yes";
    public function yell(){
        echo "Suara : Auooo";
    }
}
?>